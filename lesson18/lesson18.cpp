﻿

#include <iostream>
#include <string>
using namespace std;
int n;

class Player
{
private:
    string name;
    int points = 0;

    
public:
    Player()
    {}

    Player(string name, int points) : name(name), points(points)
    {}

    string GetName()
    {
        return this->name;
    }

    int GetPoints()
    {
        return this->points;
    }
};


void bubbleSort(Player* p)
{
    int size = n;
    for (int i = 1; i < size; size--)
    {
        for (int j = i; j < size; j++)
        {
            int left = p[j - 1].GetPoints();
            int right = p[j].GetPoints();

            if (left < right)
            {
                swap(p[j - 1], p[j]);
            }
        }
    }
}

int main()
{
    cout << "How much players do you want to add?" << endl;
    cin >> n;

    Player* playersArray = new Player[n];

    cout << "Enter name and points for each player: " << endl;
    

    // creating 
    for (int i = 0; i < n; i++) 
    {
        string name;
        int points;

        cin >> name >> points;

        Player p(name, points);
        playersArray[i] = p;
    }

    bubbleSort(playersArray);
    
    cout << endl;

    // printing sorted list
    for (int i = 0; i < n; i++)
    {
        cout << playersArray[i].GetName() << " > " << playersArray[i].GetPoints() << endl;
    }

    delete[] playersArray;
}